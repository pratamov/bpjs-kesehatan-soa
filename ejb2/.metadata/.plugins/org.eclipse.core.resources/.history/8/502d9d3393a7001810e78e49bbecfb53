package com.bpjs;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.apache.commons.codec.binary.Base64;

import com.bpjs.model.Response;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Engine.Candidate;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Fmd.Format;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;

/**
 * Session Bean implementation class FingerIdentifierBean
 */
@Stateless(name="FingerBean", mappedName="FingerBean")
@Remote
public class FingerBean implements FingerService {
	
	static {
		try{
			System.loadLibrary("dpfj");
		} catch (UnsatisfiedLinkError e){
		} catch (Exception e){}
	}
	
	private static final int VERSION = "";
	private static final int TRESHOLD_SCORE = Engine.PROBABILITY_ONE / 100;
	private static Engine engine;
	
	public FingerBean(){
		if (engine == null)
			engine = UareUGlobal.GetEngine();
	}
	
	@Override
	public Response identify(String input, String[] fingers) {
		Response response = new Response();
		
		try {
			byte[] inputByte = Base64.decodeBase64(input.getBytes());
			Fmd fmd = UareUGlobal.GetImporter().ImportFmd(
					inputByte, 
					Format.DP_VER_FEATURES,
					Format.DP_VER_FEATURES);
			
			Fmd[] fmds = new Fmd[fingers.length];
			for (int i = 0; i < fingers.length; i++){
				byte[] fingerByte = Base64.decodeBase64(fingers[i].getBytes());
				try{
					fmds[i] = UareUGlobal.GetImporter().ImportFmd(
						fingerByte, 
						Format.DP_REG_FEATURES,
						Format.DP_REG_FEATURES);
				} catch (UareUException e) {
					fmds[i] = null;
				}
			}
			
			Candidate[] matches = engine.Identify(fmd, 0, fmds, TRESHOLD_SCORE, 1);
			
			if (matches != null && matches.length > 0){
				response.setCode(200);
				response.setMessage(fingers[matches[0].fmd_index]);	
			} else {
				response.setCode(400);
				response.setMessage("Finger print is not matched");				
			}
			
		} catch (UareUException e) {
			response.setCode(500);
			response.setMessage("UareUException " + e.getLocalizedMessage());
		} catch (UnsatisfiedLinkError e) {
			response.setCode(500);
			response.setMessage("UnsatisfiedLinkError " + e.getLocalizedMessage());
		} catch (Exception e) {
			response.setCode(500);
			response.setMessage("Exception " + e.getLocalizedMessage());
		}
		
		return response;
	}

}
