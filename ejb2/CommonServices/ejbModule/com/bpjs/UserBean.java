package com.bpjs;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.apache.commons.codec.binary.Base64;

import com.berry.BCrypt;
import com.bpjs.model.Response;

@Stateless(name="UserBean", mappedName="UserBean")
@Remote
public class UserBean implements UserService {

	public static final String WS_VER_FINGER = "1.0";
	
	@Override
	public Response validatePassword(String password, String hash) {
		Response response = new Response();
		
		boolean matched = false;
		String message = "OK";
		
		if (password == null || password.trim().equals("")){
			matched = false;
			message = "Invalid password";
		} else if (hash == null || hash.trim().equals("")){
			matched = false;
			message = "Invalid hashed password";
		} else{
			matched = BCrypt.checkpw(password, hash);
			if (!matched) 
				message = "Invalid hashed password";
		}
		
		if (matched)
			response.setCode(200);
		else
			response.setCode(500);
		
		response.setMessage(message);
		
		return response;
	}

	@Override
	public Response generateToken(String hash) {
		
		Response response = new Response();
		
		if (hash == null || hash.length() == 0){
			response.setCode(400);
			response.setMessage("Invalid input");
			return response;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String date = sdf.format(new Date());

		String data = hash + "&" + date;
		
		byte[] hmacData = null;
		String s_utf = "UTF-8";
		String s_hmac = "HmacSHA256";

		try {
			byte[] b_key = WS_VER_FINGER.getBytes(s_utf);
			byte[] b_data = data.getBytes(s_utf);
			SecretKeySpec secretKey = new SecretKeySpec(b_key, s_hmac);
			Mac mac = Mac.getInstance(s_hmac);
			mac.init(secretKey);
			hmacData = mac.doFinal(b_data);
			byte[] str = Base64.encodeBase64(hmacData);
			response.setCode(200);
			response.setMessage(new String(str));
			return response;
			
		} catch (Exception e) {
			response.setCode(500);
			response.setMessage(e.getMessage());
			return response;
		}
	}

}
