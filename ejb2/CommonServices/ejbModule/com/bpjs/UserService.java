package com.bpjs;

import javax.ejb.Remote;

import com.bpjs.model.Response;

@Remote
public interface UserService {
	
	public Response validatePassword(String password, String hash);
	public Response generateToken(String hash);
	
}
