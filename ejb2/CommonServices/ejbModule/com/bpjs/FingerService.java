package com.bpjs;

import javax.ejb.Remote;

import com.bpjs.model.Response;

@Remote
public interface FingerService {

	public Response identify(String input, String[] fingers);
	
}