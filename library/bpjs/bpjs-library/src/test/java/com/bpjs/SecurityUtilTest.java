package com.bpjs;

import org.junit.Assert;
import org.junit.Test;

public class SecurityUtilTest {
	
	@Test
	public void testGenerateToken() {
		String hashedPassword = "$2a$10$0r.bi9KV/0UYvlz2XgUABevR3eL5LW1RvRrtnaF1mzmvEb5LKbbUC";
		String token = SecurityUtil.generateToken(hashedPassword);		
		Assert.assertNotNull(token);
	}
	
	@Test
	public void testValidatePassword() {
		String password = "password@A1";
		String hashedPassword = "$2a$10$0r.bi9KV/0UYvlz2XgUABevR3eL5LW1RvRrtnaF1mzmvEb5LKbbUC";
		boolean valid = SecurityUtil.validatePassword(password, hashedPassword);
		
		Assert.assertTrue(valid);
	}
	
}
