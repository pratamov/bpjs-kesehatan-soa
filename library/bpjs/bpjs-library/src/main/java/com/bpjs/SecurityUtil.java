package com.bpjs;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.berry.BCrypt;

public class SecurityUtil {
	
	public static final String WS_VER_FINGER = "1.0";

	public static String generateToken(String hashedPassword) {

		if (hashedPassword == null || hashedPassword.length() == 0)
			return "";

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String date = sdf.format(new Date());

		String data = hashedPassword + "&" + date;
		
		byte[] hmacData = null;
		String s_utf = "UTF-8";
		String s_hmac = "HmacSHA256";

		try {
			byte[] b_key = WS_VER_FINGER.getBytes(s_utf);
			byte[] b_data = data.getBytes(s_utf);
			SecretKeySpec secretKey = new SecretKeySpec(b_key, s_hmac);
			Mac mac = Mac.getInstance(s_hmac);
			mac.init(secretKey);
			hmacData = mac.doFinal(b_data);
			byte[] str = Base64.encodeBase64(hmacData);
			return new String(str);
			
		} catch (Exception e) {
			return "";
		}
		
	}

	public static boolean validatePassword(String password, String hashedPassword) {
		
		if (password == null || password.trim().equals(""))
			return false;
		
		if (hashedPassword == null || hashedPassword.trim().equals(""))
			return false;
		
		return BCrypt.checkpw(password, hashedPassword);

	}

}
