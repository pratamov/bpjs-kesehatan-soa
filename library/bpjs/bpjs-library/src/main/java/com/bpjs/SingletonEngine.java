package com.bpjs;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.UareUGlobal;
import com.digitalpersona.uareu.dpfj.EngineImpl;

import oracle.ias.cache.CacheAccess;
import oracle.ias.cache.CacheException;
import oracle.ias.cache.ObjectNotFoundException;

public class SingletonEngine {
	
	private SingletonEngine() {}
	
	public static synchronized Engine getInstance() throws UnsatisfiedLinkError, CacheException {
		
		CacheAccess cacc = CacheAccess.getAccess();
		Engine engine = null;
		try {
			engine = (EngineImpl) cacc.get("dpuareu_engine");
		} catch (ObjectNotFoundException e) {}
		if (engine == null) {
			engine = build();
			cacc.put("dpuareu_engine", engine);
		}
		cacc.close();
		
		return engine;
		
	}
	
	private static Engine build() throws UnsatisfiedLinkError {
		
		loadLibraries();
		Engine engine = UareUGlobal.GetEngine();
		return engine;
		
	}
	
	private static void loadLibraries() {
		
		try {
			System.loadLibrary("dpfj");
			System.out.println("dpfj loaded");
		} catch (UnsatisfiedLinkError e) {
		}

		try {
			System.loadLibrary("dpfpdd");
			System.out.println("dpfpdd loaded");
		} catch (UnsatisfiedLinkError e) {
		}
		
	}
	
}
