package com.bpjs;

import java.util.List;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Engine.Candidate;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;

import oracle.ias.cache.CacheException;

import com.digitalpersona.uareu.Fmd.Format;

import org.apache.commons.codec.binary.Base64;

public class FingerPrintUtil {

	private static final int TRESHOLD_SCORE = Engine.PROBABILITY_ONE / 100;

	public static Fmd convertToFmd(String finger, Format format) throws UareUException {
		
		byte[] fingerPrintByte = Base64.decodeBase64(finger.getBytes());
		
		return UareUGlobal.GetImporter().ImportFmd(
				fingerPrintByte, 
				format,
				format);
		
	}
	
	public static Candidate matches(String finger, List<String> fingers) throws UareUException, UnsatisfiedLinkError, CacheException {

		Engine engine = SingletonEngine.getInstance();
		
		try {
			Fmd fmd = convertToFmd(finger.trim(), Format.DP_VER_FEATURES);
			Fmd[] fmds = new Fmd[fingers.size()];
			for (int i = 0; i < fingers.size(); i++) {
				fmds[i] = convertToFmd(fingers.get(i).trim(), Format.DP_REG_FEATURES);
			}
			Candidate[] matches = engine.Identify(fmd, 0, fmds, TRESHOLD_SCORE, 1);
			
			if (matches.length == 1) {
				Candidate candidate = matches[0];
				return candidate;
			}
		} catch (UareUException e) {
			throw e;
		}
		
		return null;
	}
	
	
}
