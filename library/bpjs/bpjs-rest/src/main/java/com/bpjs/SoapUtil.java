package com.bpjs;

import java.util.Map;

public class SoapUtil {
	
	public static String JsonToXml(String bodyJson, String bodyTag) {
		
		SoapEnvelope soapEnvelope = new SoapEnvelope(bodyJson, null, bodyTag, null);
		return soapEnvelope.getMessageXml();
		
	}
	
	public static String JsonToXml(String bodyJson, Map<String, String> headers, String bodyTag, String headerTag) {
		
		SoapEnvelope soapEnvelope = new SoapEnvelope(bodyJson, headers, bodyTag, headerTag);
		return soapEnvelope.getMessageXml();
		
	}
	
	public static String XmlToJson(String soap) {
		
		SoapEnvelope soapEnvelope = new SoapEnvelope(soap);
		return soapEnvelope.getBodyJson();

	}
	
	
}
