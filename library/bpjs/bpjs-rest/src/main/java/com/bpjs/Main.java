package com.bpjs;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlObject.Factory;

public class Main {

	public static void main(String[] args) {
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("token", "toktoktok");
		
		//String xml = "<soapenv:Envelope xmlns=\"http://www.example.org\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body ><TestRequest ><a >AOhMAcgp43NcwEE381mKK5RcZ2bYb4Kn8K5EFETMhdEPIdMQm0DmKqXLGtU/EVaTEYCMPh/mRutqiZIfVKWC3gJDMxisjfq2h646GCo9UD59wsqWHVWtZJrSmVK36ovsfvTtrPnHra/9Os0Erw8y+tuqtAWtHJwG5dqcE953r8CmGD1F2Y7YLGE+SQpYSJq4ldCLXvWBr/+N9UapYt9irxJBEnNR2SO4epQEC2qjVcx1mDKPdROpH5LhMsJX5jkavrCgBIin5BfwQplNNCT9mg5ZIt0Ik5jFH1vyZqUrEordXU89l/+4ll5GzttMF2MtcnRWX+v2AWveS6Tp9tdTQiyhdB7JJ6uxep8tPEMFGZyhR61+gePZXqgsnGJDGJjGitmkPXrfSLA7N05GIzrDPXrRZUu0Sn2QaycbO9NBlR2COL7I1gyl0HOfXLKn8KdSbw==</a><b >APiTAcgq43NcpFQ7Tx0bXQ5pXxOzJjmkVrlSmoE/pmWoQGwCvqNwv3RYTbUNI/jHKXdKMfNuTdKWkUK6HpJ/s6cRLCBm18T/jPb8vDDYEcpADEMy4ydQAQ8wusJQ9XYA9abodwyojmP3csuTnXNm5+yHOLJDtfcBEFLoQu1gmzveuC29dRSSGSh3/paA3KYf05Hc7AjiWdVeyQrtPyfr5hbdpbZ5KFcwHstqqQ7ljIU28raiUs9OnrNGli2QTIerxIzEcHpSMOT1JGUQ2vvbqKUUcam174hr6avGPjsbiy24IpTTgHa/ldUoOzJqbFWFDa7xhqEIiM2LAylbPb5ulLcJGwhc5LhiMpI+Hd0wQOev4DEzNoFwzXhlqnZz5h0LuUM0Ollz1U6aXl6kKErTa0WPEQJ9o3NVvFqouHzmrPZ4LfV+PQynAc04ptgMOMvmueCAkah+W1Dtf/oeAfg9GPZK3fHIu09l72QsvfUatGio8ugcrsJluadz2Sl6scQepH+1ZeAN7L0oef9foaEp1fGiDNI32v1vAPhvAcgq43Ncj1Mi7l8p/e2zIFou0hB+ANPJHp0bS5bKAvaacQHrgFvZtQscjybDh97dcc9TdSCPzKW3zW0969SF9zYi8E83hMY5VBBUxJJrZ0YkWWrR9YEUfR24NTpIY4MyKKNsuriVhTtpRivZUlu7thfjQrVQWQ4xoteJoLgkqSU7ylmiPRwl9K70nhrXA6qqI+iFvX4wk6HEZtUrGBk5g5ODQd3fp/Ljwf2PuKJOFwT1h1uVwQgPhFZ5hIwfT8/AsriPsZg/wGDkw/XpbSG+8A4b7FpZKmuBLTGIuXKuQzUZMGU675SylQiuzzbKrBcTcWURktLqrASrhE8Cgx0+Paj3irWTinx3NphNaauqvZYhopYEUMYPALk0E08JMZkUKvLD7gOnfhA9zeZRrFFFmZ4W86mrUrAVhFaXYdd3z7P65OkL4zjhrovxt4oniZtSQ2zOjzF6q4SadyLUSPTfeVycG2pIokoBrWISR2wqAipvAPh0Acgq43NcvlQ4gOm2aaYq1o6SmotXqg3JlHCDUgtel8a0V4eub8LlCkDz5VS8hHJ5ZQfevGJfVFOxXluXK3Ny1w/MdfbqEZLxynrOBDXsKxnNmkJUHw2SQVyC9WMM2bnCosFeA1l0jlV+OljlTOmkUIYpFxzCbpnB+aBKIhypyH+R/ZmqmRHF89C++hMEJ4IilU6y9/FhefJcXgr1RDdlPu+uKqbwyAWBsSxSShpGMZdzaEloYFYlpKEwVaKbNgSOibmjfS2BdE7IZugmTzNZGZe1MbCfk5GbBkAefd4ZByDMgFH6r9bYop1qv9Ct8ASb6XjW5/MVymup67nYeK50FazGixMC5jsnn9xjotPHhynTWnu2zSz+JCjI6dFBECT7UwHq7N44tKDPaGitPdzLfueFi6z6TjvqRljp1AkR/najsfoVWTcdFKe/p2wHkL0C00EVWCIdLXYHGU3JQkE9LBVurhrc2pKXFUSja/hoF5KhgTBr+28A6GAByCrjc1y4UyfXZerpiDNdeKnwESdwA7dR8+JNT2QY0THGS4GYot/2BF7WUx2kKTBr3tUxbNXTv89py3LbDPKIGHXCGTz/BNkn7tUZGhT9nDjPxYwgwbCk7D8vKam9cUguVFmqbiHfsbAwtag870nPeduqIco1lFDIgoRkDQRT08gnZylLdtl1QB/sVGA6svmI+hhfONo8oj9xGaAN4TLCqp4pfBu82YJrjsmpf2l+mgmHHM3SF5SQfJQGQxplDd2OLNfdcBXtuwfyaNcGZuk1pXrgfpZBmEVLlywJIpKvN8Ve0cIyLNDh230PU5Vne56RWIlAGPB56qwFfKro44bprJNDLHuKz0GvCN23vOPGEaylDeuxhtGyOSIAQiBuVSD3MiRLc12v0YTz5yitdBRVuf5qqpBQLm//JS0j3Pi0ge0lCxTuihkzmfM8rRnBVT0JXtBMdXzRolZKIxL9rDK1WW8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</b></TestRequest></soapenv:Body></soapenv:Envelope>";
		//String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response xmlns=\"http://www.example.org\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\"><c>UareUException null</c><d/></Response>";
		String json = "{\"nokapst\":\"0000000000001\",\"finger\":[{\"finger\":\"FINGER\",\"index\":1},{\"finger\":\"FINGER\",\"index\":2},{\"finger\":\"FINGER\",\"index\":10}]}";
		String xml = "<response><metaData><message>asd</message><code>401</code></metaData></response>";
		
		XmlObject xmlRes = SoapEnvelope.jsonToXml(json, "Request");
		String jsonRes;
		
		try {
			jsonRes = SoapEnvelope.xmlToJson(Factory.parse(xml));
			System.out.println(xmlRes);
			//System.out.println(SoapEnvelope.jsonToXml(json, "Request"));
		} catch (XmlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String test = SoapUtil.JsonToXml("{\"aa\": \"zxc\",\"ab\": {\"a\":\"asd\",\"b\":\"qwe\"}}", headers, "TestRequest", "TestResponse");
		//String test = SoapUtil.XmlToJson("<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns=\"http://www.example.org\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body ><TestRequest ><aa >zxc</aa><ab ><a >asd</a><b >qwe</b></ab></TestRequest></soapenv:Body><soapenv:Header ><TestResponse ><token >toktoktok</token></TestResponse></soapenv:Header></soapenv:Envelope>");
		

	}

}
