package com.bpjs;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlObject.Factory;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.impl.values.XmlAnyTypeImpl;

import de.odysseus.staxon.json.JsonXMLConfig;
import de.odysseus.staxon.json.JsonXMLConfigBuilder;
import de.odysseus.staxon.json.JsonXMLInputFactory;
import de.odysseus.staxon.xml.util.PrettyXMLEventWriter;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

public class SoapEnvelope {
	
	private final String NAMESPACE_URL = "http://www.example.org";
	private JSONObject header;
	private JSONObject body;
	private JSONObject message;
	
	public SoapEnvelope(String xml) {
		
		this.parseXml(xml);
		
	}
	
	public SoapEnvelope(String bodyJson, Map<String, String> headers) {
		
		this.buildHeader(headers, "Header");
		this.buildBody(bodyJson, "Body");
		this.buildMessage(body, header);
		
	}
	
	public SoapEnvelope(String bodyJson, Map<String, String> headers, String bodyTag, String headerTag) {
		
		this.buildHeader(headers, headerTag);
		this.buildBody(bodyJson, bodyTag);
		this.buildMessage(body, header);
		
	}
	
	private void buildHeader(Map<String, String> headers, String tag) {
		
		if (headers == null || headers.isEmpty())
			return;
		
		JSONObject parent = new JSONObject();
		JSONObject element = new JSONObject();
		element.putAll(headers);
		parent.put(tag, element);
		
		this.header = parent;
		
	}
	
	private void buildBody(String body, String tag) {
		
		JSONObject parent = new JSONObject();
		JSONObject element = JSONObject.fromObject(body);
		parent.put(tag, element);
		
		this.body = parent;
		
	}
	
	private void buildMessage(JSONObject body, JSONObject header) {
		
		JSONObject parent = new JSONObject();
		parent.put("soapenv:Body", body);
		if (header != null)
			parent.put("soapenv:Header", header);
		
		this.message = parent;
		
	}
	
	private void parseXml(String xml) {
		
		XMLSerializer xmlSerializer = new XMLSerializer();
		xmlSerializer.setRemoveNamespacePrefixFromElements(true);
		xmlSerializer.setSkipNamespaces(true);
		
		JSONArray json = (JSONArray) xmlSerializer.read(xml);
		
		System.out.println(json);
		
		//this.message = json;
		//this.body = (JSONObject) json.get("Body");
		//this.header = (JSONObject) json.get("Header");
		
	}
	
	public String getBodyJson() {
		
		if (this.body == null) {
			return "{}";
		}
		
		String key = "";
		Iterator<?> it = this.body.keys();
		while(it.hasNext()) {
			key = (String) it.next();
		}
		
		JSONObject payload = this.body.getJSONObject(key);
		return payload.toString();
		
	}
	
	public String getHeaderJson() {
		if (this.header == null)
			return "{}";
		
		String key = "";
		Iterator<?> it = this.header.keys();
		while(it.hasNext()) {
			key = (String) it.next();
		}
		
		JSONObject payload = this.header.getJSONObject(key);
		return payload.toString();
	}
	
	public String getMessageJson() {
		if (this.message == null)
			return "{}";
		
		return this.message.toString();
	}
	
	public String getHeaderXml() {
		
		XMLSerializer xmlSerializer = new XMLSerializer();
		xmlSerializer.setTypeHintsEnabled(false);
		xmlSerializer.setRemoveNamespacePrefixFromElements(true);
		return xmlSerializer.write(this.header);
		
	}
	
	public String getBodyXml() {
		XMLSerializer xmlSerializer = new XMLSerializer();
		xmlSerializer.setTypeHintsEnabled(false);
		xmlSerializer.setRemoveNamespacePrefixFromElements(true);
		return xmlSerializer.write(this.body);
	}
	
	public String getMessageXml() {
		
		XMLSerializer xmlSerializer = new XMLSerializer();
		xmlSerializer.setTypeHintsEnabled(false);
		xmlSerializer.setRootName("soapenv:Envelope");
		xmlSerializer.setNamespace(null, NAMESPACE_URL);
		xmlSerializer.addNamespace("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		xmlSerializer.setRemoveNamespacePrefixFromElements(false);
		
		return xmlSerializer.write(this.message);
		
	}

	@Override
	public String toString() {
		return this.getMessageXml();
	}
	
	public static XmlObject jsonToXml(String payload, String tag) {
		try {
			
			JsonXMLConfig jsonXMLConfig = new JsonXMLConfigBuilder()
	        		.virtualRoot(tag)
	                .multiplePI(false)
	                .build();
	        
	        StringWriter strWriter = new StringWriter();
	        InputStream inputStream = new ByteArrayInputStream(payload.getBytes());
	        
			XMLEventReader xmlEventReader = new JsonXMLInputFactory(jsonXMLConfig)
			        .createXMLEventReader(inputStream);
			XMLEventWriter xmlEventWriter = XMLOutputFactory.newInstance()
	                .createXMLEventWriter(strWriter);
	        
	        xmlEventWriter = new PrettyXMLEventWriter(xmlEventWriter);
	        xmlEventWriter.add(xmlEventReader);
	        String xmlData = strWriter.getBuffer().toString();
	        
	        xmlData = xmlData.replaceFirst("<" + tag + ">", "<" + tag + " xmlns=\"http://www.example.org\">");
	        
	        XmlOptions options = new XmlOptions().setSaveNoXmlDecl();
			XmlObject xml = Factory.parse(xmlData, options);
			return xml;
	        
		} catch (XMLStreamException e1) {
			return null;
		} catch (javax.xml.stream.FactoryConfigurationError e1) {
			return null;
		} catch (XmlException e) {
			return null;
		}
		
	}
	
	public static String xmlToJson(XmlObject xml) {
		
		SoapEnvelope envelope = new SoapEnvelope(xml.toString());
		return envelope.getMessageJson();
		
	}
	
	public static String xmlToJson(String xml) {
		
		try {
			XmlObject xmlObject = Factory.parse(xml);
			SoapEnvelope envelope = new SoapEnvelope(xmlObject.toString());
			return envelope.getMessageJson();
		} catch (XmlException e) {
			return "{}";
		}
		
	}
	
	
}
