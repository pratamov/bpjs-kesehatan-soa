# README

## Development Tools

| Project Type | IDE | Version |
| -------- | -------- | -------- |
| SOA Composite Project     | Oracle JDeveloper     | 11.1.1.9.0     |
| Maven Project     | Eclipse     | Oxygen.2 (4.7.2)     |
| Service Bus Project | Oracle Enterprise Pack for Eclipse | 11.1.1.8.0 |

## Project Structure

```
project
│   README.md 
│
└───soa
│	└───Denda (SOA Composite Project)
│	└───FingerPrint (SOA Composite Project)
│   
└───library
│	└───Denda (Maven Project)
│	└───FingerPrint (Maven Project)
│   
└───osb
	└───Denda (Service Bus Project)
	└───FingerPrint (Service Bus Project)
```

## Datasources

| Datasource Name | JDBC URL | JNDI Name | EIS JNDI Name |
| -------- | -------- | -------- | -------- |
| CollectPremiDS | jdbc:sqlserver://172.23.5.129:1433;databaseName=CollectPremi | jdbc/bpjs/collectpremi | eis/bpjs/collectpremi |
| RegistrasiDS | jdbc:sqlserver://172.23.5.129:1433;databaseName=Registrasi | jdbc/bpjs/registrasi | eis/bpjs/registrasi |
| dbVedikaDS | jdbc:sqlserver://172.23.5.129:1433;databaseName=dbVedika | jdbc/bpjs/dbvedika | eis/bpjs/dbvedika |
| DBKepesertaanDS | jdbc:sqlserver://172.23.5.129:1433;databaseName=DBKepesertaan | jdbc/bpjs/dbkepesertaan | eis/bpjs/dbkepesertaan |

## Architecture

## Components

### Denda

### FingerPrint