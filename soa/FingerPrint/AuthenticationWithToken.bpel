<?xml version = "1.0" encoding = "UTF-8" ?>
<!--
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  Oracle JDeveloper BPEL Designer 
  
  Created: Mon Aug 13 20:45:36 ICT 2018
  Author:  gimel
  Type: BPEL 2.0 Process
  Purpose: Synchronous BPEL Process
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-->
<process name="AuthenticationWithToken"
               targetNamespace="http://xmlns.oracle.com/Application1/FingerPrint/AuthenticationWithToken"
               xmlns="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
               xmlns:client="http://xmlns.oracle.com/Application1/FingerPrint/AuthenticationWithToken"
               xmlns:ora="http://schemas.oracle.com/xpath/extension"
               xmlns:bpelx="http://schemas.oracle.com/bpel/extension"
         xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
         xmlns:ns1="http://xmlns.oracle.com/pcbpel/adapter/db/Application1/FingerPrint/usp_is_valid_token"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:ns2="http://www.example.org"
         xmlns:ns3="http://xmlns.oracle.com/pcbpel/adapter/db/sp/usp_is_valid_token"
         xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
         xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/"
         xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
         xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
         xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath"
         xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath"
         xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions"
         xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk"
         xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
         xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap">

    <import namespace="http://xmlns.oracle.com/Application1/FingerPrint/AuthenticationWithToken" location="AuthenticationWithToken.wsdl" importType="http://schemas.xmlsoap.org/wsdl/"/>
    <!-- 
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        PARTNERLINKS                                                      
        List of services participating in this BPEL process               
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    --> 
  <partnerLinks>
    <!-- 
      The 'client' role represents the requester of this service. It is 
      used for callback. The location and correlation information associated
      with the client role are automatically set using WS-Addressing.
    -->
    <partnerLink name="authenticationwithtoken_client" partnerLinkType="client:AuthenticationWithToken" myRole="AuthenticationWithTokenProvider"/>
    <partnerLink name="usp_is_valid_token"
                 partnerLinkType="ns1:usp_is_valid_token_plt"
                 partnerRole="usp_is_valid_token_role"/>
  </partnerLinks>

  <!-- 
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      VARIABLES                                                        
      List of messages and XML documents used within this BPEL process 
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  -->
  <variables>
    <!-- Reference to the message passed as input during initiation -->
    <variable name="inputVariable" messageType="client:AuthenticationWithTokenRequestMessage"/>

    <!-- Reference to the message that will be returned to the requester-->
    <variable name="outputVariable" messageType="client:AuthenticationWithTokenResponseMessage"/>
    <variable name="CheckToken_usp_is_valid_token_InputVariable"
              messageType="ns1:args_in_msg"/>
    <variable name="CheckToken_usp_is_valid_token_OutputVariable"
              messageType="ns1:args_out_msg"/>
  </variables>

  <!-- 
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     ORCHESTRATION LOGIC                                               
     Set of activities coordinating the flow of messages across the    
     services integrated within this business process                  
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  -->
  <sequence name="main">

    <!-- Receive input from requestor. (Note: This maps to operation defined in AuthenticationWithToken.wsdl) -->
    <receive name="receiveInput" partnerLink="authenticationwithtoken_client" portType="client:AuthenticationWithToken" operation="process" variable="inputVariable" createInstance="yes"/>
    <assign name="AssignParameters">
      <copy ignoreMissingFromData="yes">
        <from>$inputVariable.payload/ns2:token</from>
        <to>$CheckToken_usp_is_valid_token_InputVariable.InputParameters/ns3:token</to>
      </copy>
      <copy ignoreMissingFromData="yes">
        <from>$inputVariable.payload/ns2:kdapp</from>
        <to>$CheckToken_usp_is_valid_token_InputVariable.InputParameters/ns3:kdapp</to>
      </copy>
    </assign>
    <invoke name="CheckToken" bpelx:invokeAsDetail="no"
            partnerLink="usp_is_valid_token"
            portType="ns1:usp_is_valid_token_ptt"
            operation="usp_is_valid_token"
            inputVariable="CheckToken_usp_is_valid_token_InputVariable"
            outputVariable="CheckToken_usp_is_valid_token_OutputVariable"/>
    <if name="CheckResponse">
      <documentation>
        <![CDATA[NoError]]>
      </documentation>
      <condition>$CheckToken_usp_is_valid_token_OutputVariable.OutputParameters/ns3:error = ''</condition>
      <assign name="AssignResponse">
        <copy>
          <from>$CheckToken_usp_is_valid_token_OutputVariable.OutputParameters/ns3:username</from>
          <to>$outputVariable.payload/ns2:response/ns2:username</to>
        </copy>
        <copy>
          <from><literal>200</literal></from>
          <to>$outputVariable.payload/ns2:metaData/ns2:code</to>
        </copy>
        <copy>
          <from><literal>OK</literal></from>
          <to>$outputVariable.payload/ns2:metaData/ns2:message</to>
        </copy>
      </assign>
      <else>
        <documentation>
          <![CDATA[Error]]>
        </documentation>
        <assign name="AssignResponse">
          <copy>
            <from><literal>401</literal></from>
            <to>$outputVariable.payload/ns2:metaData/ns2:code</to>
          </copy>
          <copy>
            <from>$CheckToken_usp_is_valid_token_OutputVariable.OutputParameters/ns3:error</from>
            <to>$outputVariable.payload/ns2:metaData/ns2:message</to>
          </copy>
        </assign>
      </else>
    </if>
    <!-- Generate reply to synchronous request -->
    <reply name="replyOutput" partnerLink="authenticationwithtoken_client" portType="client:AuthenticationWithToken" operation="process" variable="outputVariable"/>
  </sequence>
</process>