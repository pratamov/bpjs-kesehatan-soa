package com.bpjs;

import com.bpjs.model.Response;

public interface FingerService {

	public Response identify(String input, String[] fingers);
	
}
