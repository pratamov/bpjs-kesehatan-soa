package com.bpjs;

import com.bpjs.model.Response;

public interface UserService {
	
	public Response validatePassword(String password, String hash);
	public Response generateToken(String hash);
	
}
